package safe_sqlite

import "database/sql"

type locked_db struct {
	db IExecutor
}

//lint:ignore U1000 reason: compile time verification
func (self locked_db) _verify() ICursor {
	return self
}

func (self locked_db) WithRaw(f func(IExecutor) error) error {
	return f(self.db)
}

func (self locked_db) Exec(query string, args ...any) error {
	_, err := self.db.Exec(query, args...)
	return err
}

func (self locked_db) ExecReturnId(query string, args ...any) (int64, error) {
	res, err := self.db.Exec(query, args...)
	if err != nil {
		return 0, err
	}
	return res.LastInsertId()
}

func (self locked_db) Iter1(f func(*sql.Rows) error, query string, args ...any) error {
	rows, err := self.db.Query(query, args...)
	if err != nil {
		return err
	}
	defer rows.Close()
	if rows.Next() {
		err := f(rows)
		if err != nil {
			return err
		}
	}
	return nil
}

func (self locked_db) IterMany(f func(*sql.Rows) error, query string, args ...any) error {
	rows, err := self.db.Query(query, args...)
	if err != nil {
		return err
	}
	defer rows.Close()
	for rows.Next() {
		err := f(rows)
		if err != nil {
			return err
		}
	}
	return nil
}
