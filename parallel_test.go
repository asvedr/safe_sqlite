package safe_sqlite_test

import (
	"database/sql"
	"reflect"
	"strings"
	"time"

	ss "gitlab.com/asvedr/safe_sqlite"

	"sync"
	"testing"
)

func worker(wg *sync.WaitGroup, db ss.ICursor, id int) {
	vals, err := ss.FetchSlice(
		db,
		func(r *sql.Rows) (string, error) {
			var val string
			err := r.Scan(&val)
			time.Sleep(time.Millisecond * 10)
			return val, err
		},
		"SELECT val FROM tbl WHERE key = ?",
		id,
	)
	if err != nil {
		panic(err.Error())
	}
	time.Sleep(time.Millisecond * 10)
	res := strings.Join(vals, "")
	time.Sleep(time.Millisecond * 10)
	err = db.Exec("INSERT INTO report (id, result) VALUES (?, ?)", id, res)
	time.Sleep(time.Millisecond * 10)
	if err != nil {
		panic(err.Error())
	}
	wg.Done()
}

func TestMultipleRoutines(t *testing.T) {
	db := setup()
	db.Exec(`
		CREATE TABLE report (
			id integer,
			result string
		)
	`)
	for i := 0; i < 3; i += 1 {
		key := string(rune(int('a') + i))
		val := string(rune(int('a') + (i * 5)))
		db.Exec(
			"INSERT INTO tbl (key, val) VALUES (?, ?), (?, ?)",
			i,
			key,
			i,
			val,
		)
	}
	var wg sync.WaitGroup
	wg.Add(3)
	go worker(&wg, db, 0)
	go worker(&wg, db, 1)
	go worker(&wg, db, 2)
	wg.Wait()
	got, err := ss.FetchMap(
		db,
		func(r *sql.Rows) (int, string, error) {
			var id int
			var res string
			err := r.Scan(&id, &res)
			return id, res, err
		},
		"SELECT id, result FROM report",
	)
	if err != nil {
		t.Fatal(err)
	}
	expected := map[int]string{
		0: "aa",
		1: "bf",
		2: "ck",
	}
	if !reflect.DeepEqual(got, expected) {
		t.Fatal(got)
	}
}
