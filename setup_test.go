package safe_sqlite_test

import ss "gitlab.com/asvedr/safe_sqlite"

func setup() ss.IDb {
	db, err := ss.New(":memory:")
	if err != nil {
		panic(err.Error())
	}
	query := `CREATE TABLE tbl (
		id integer primary key not null,
		key string,
		val string
	)`
	err = db.Exec(query)
	if err != nil {
		panic(err.Error())
	}
	return db
}
