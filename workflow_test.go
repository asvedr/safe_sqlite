package safe_sqlite_test

import (
	"database/sql"
	"errors"
	"fmt"
	"reflect"
	"testing"

	ss "gitlab.com/asvedr/safe_sqlite"
)

func f_ExecReturnId(db ss.ICursor, t *testing.T) {
	id, err := db.ExecReturnId("insert into tbl (key, val) VALUES (?, ?)", "a", "'")
	if err != nil {
		t.Fatal(err)
	}
	f := func(r *sql.Rows) ([]string, error) {
		var key, val string
		err := r.Scan(&key, &val)
		return []string{key, val}, err
	}
	row, err := ss.FetchOne(db, f, "SELECT key, val FROM tbl WHERE id = ?", id)
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(row, &[]string{"a", "'"}) {
		t.Fatal(row)
	}
	row, err = ss.FetchOne(db, f, "SELECT key, val FROM tbl WHERE id = ?", id+100)
	if err != nil || row != nil {
		t.Fatalf("%v|%v", row, err)
	}
}

func TestExecReturnId(t *testing.T) {
	db := setup()
	f_ExecReturnId(db, t)
	f := func(l ss.ICursor) error {
		f_ExecReturnId(l, t)
		return nil
	}
	db.WithWLocked(f)
}

func f_FetchSlice(db ss.ICursor, t *testing.T) {
	db.Exec(
		"INSERT INTO tbl (key, val) VALUES (?, ?), (?, ?)",
		1, "a",
		2, "b",
	)
	f := func(r *sql.Rows) (string, error) {
		var key, val string
		err := r.Scan(&key, &val)
		return fmt.Sprintf("%v:%v", key, val), err
	}
	vals, err := ss.FetchSlice(db, f, "SELECT key, val FROM tbl ORDER BY key")
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(vals, []string{"1:a", "2:b"}) {
		t.Fatal(vals)
	}
}

func TestFetchSlice(t *testing.T) {
	f_FetchSlice(setup(), t)
	f := func(l ss.ICursor) error {
		f_FetchSlice(l, t)
		return nil
	}
	setup().WithWLocked(f)
}

func f_FetchMap(db ss.ICursor, t *testing.T) {
	err := db.Exec(
		"INSERT INTO tbl (key, val) VALUES (?, ?), (?, ?)",
		1, "a",
		2, "b",
	)
	if err != nil {
		t.Fatal(err)
	}
	f := func(r *sql.Rows) (string, string, error) {
		var key, val string
		err := r.Scan(&key, &val)
		return key, val, err
	}
	vals, err := ss.FetchMap(db, f, "SELECT key, val FROM tbl ORDER BY key")
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(vals, map[string]string{"1": "a", "2": "b"}) {
		t.Fatal(vals)
	}
}

func TestFetchMap(t *testing.T) {
	f_FetchMap(setup(), t)
	f := func(l ss.ICursor) error {
		f_FetchMap(l, t)
		return nil
	}
	setup().WithWLocked(f)
}

func TestTxCommit(t *testing.T) {
	db := setup()

	db.WithTxLocked(func(l ss.ICursor) error {
		f_FetchMap(l, t)
		return nil
	})

	f := func(r *sql.Rows) (string, string, error) {
		var key, val string
		err := r.Scan(&key, &val)
		return key, val, err
	}
	vals, err := ss.FetchMap(db, f, "SELECT key, val FROM tbl ORDER BY key")
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(vals, map[string]string{"1": "a", "2": "b"}) {
		t.Fatal(vals)
	}
}

func TestTxRollback(t *testing.T) {
	db := setup()

	db.WithTxLocked(func(l ss.ICursor) error {
		f_FetchMap(l, t)
		return errors.New("some error")
	})

	f := func(r *sql.Rows) (string, string, error) {
		var key, val string
		err := r.Scan(&key, &val)
		return key, val, err
	}
	vals, err := ss.FetchMap(db, f, "SELECT key, val FROM tbl ORDER BY key")
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(len(vals), 0) {
		t.Fatal(vals)
	}
}
