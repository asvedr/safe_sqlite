package safe_sqlite

import "database/sql"

type ICursor interface {
	WithRaw(f func(IExecutor) error) error
	Exec(query string, args ...any) error
	ExecReturnId(query string, args ...any) (int64, error)
	IterMany(f func(*sql.Rows) error, query string, args ...any) error
	Iter1(f func(*sql.Rows) error, query string, args ...any) error
}

type IDb interface {
	ICursor
	// mutex lock-read
	// WithRLocked(f func(ICursor) error) error
	// rlock is deprecated

	// mutex lock-write
	WithWLocked(f func(ICursor) error) error
	// mutex lock-write + create transaction
	WithTxLocked(f func(ICursor) error) error
}

type IExecutor interface {
	Exec(query string, args ...any) (sql.Result, error)
	Query(query string, args ...any) (*sql.Rows, error)
}
