package safe_sqlite

import (
	"database/sql"
	"sync"

	_ "modernc.org/sqlite"
)

type base_db struct {
	db  *sql.DB
	mtx *sync.Mutex
}

//lint:ignore U1000 reason: compile time verification
func (self *base_db) _verify() IDb {
	return self
}

func New(path string) (IDb, error) {
	src, err := sql.Open("sqlite", path)
	if err != nil {
		return nil, err
	}
	return &base_db{db: src, mtx: &sync.Mutex{}}, nil
}

func (self *base_db) WithTxLocked(f func(ICursor) error) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	tx, err := self.db.Begin()
	if err != nil {
		return err
	}
	err = f(locked_db{db: tx})
	if err == nil {
		err = tx.Commit()
	}
	if err != nil {
		tx.Rollback()
	}
	return err
}

func (self *base_db) WithWLocked(f func(ICursor) error) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	return f(locked_db{db: self.db})
}

func (self *base_db) WithRaw(f func(IExecutor) error) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	return f(self.db)
}

func (self *base_db) Exec(query string, args ...any) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	_, err := self.db.Exec(query, args...)
	return err
}

func (self *base_db) ExecReturnId(query string, args ...any) (int64, error) {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	res, err := self.db.Exec(query, args...)
	if err != nil {
		return 0, err
	}
	return res.LastInsertId()
}

func (self *base_db) Iter1(f func(*sql.Rows) error, query string, args ...any) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	rows, err := self.db.Query(query, args...)
	if err != nil {
		return err
	}
	defer rows.Close()
	if rows.Next() {
		err := f(rows)
		if err != nil {
			return err
		}
	}
	return nil
}

func (self *base_db) IterMany(f func(*sql.Rows) error, query string, args ...any) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	rows, err := self.db.Query(query, args...)
	if err != nil {
		return err
	}
	defer rows.Close()
	for rows.Next() {
		err := f(rows)
		if err != nil {
			return err
		}
	}
	return nil
}
