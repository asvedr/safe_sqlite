package safe_sqlite

import "database/sql"

func FetchOne[T any](
	db ICursor,
	deser func(*sql.Rows) (T, error),
	query string,
	args ...any,
) (*T, error) {
	var result *T
	f := func(r *sql.Rows) error {
		val, err := deser(r)
		result = &val
		return err
	}
	err := db.Iter1(f, query, args...)
	return result, err
}

func FetchSlice[T any](
	db ICursor,
	deser func(*sql.Rows) (T, error),
	query string,
	args ...any,
) ([]T, error) {
	var result []T
	f := func(r *sql.Rows) error {
		val, err := deser(r)
		if err != nil {
			return err
		}
		result = append(result, val)
		return nil
	}
	err := db.IterMany(f, query, args...)
	return result, err
}

func FetchMap[K comparable, T any](
	db ICursor,
	deser func(*sql.Rows) (K, T, error),
	query string,
	args ...any,
) (map[K]T, error) {
	result := make(map[K]T)
	f := func(r *sql.Rows) error {
		k, v, err := deser(r)
		if err != nil {
			return err
		}
		result[k] = v
		return nil
	}
	err := db.IterMany(f, query, args...)
	return result, err
}
